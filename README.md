# Storage container for objectives

## Project structure

```bash
This project
    ├── assets # create new folders if needed
    │   ├── drawings # technical drawings
    │   ├── images # pictures or screenshots of the parts
    │   └── models # 2d/3d rendering of parts
    └── src # actual .stl source files
```

## One sentence pitch

![](assets/images/Objective_storage__Lightsheet_.jpeg)

Robust container to safely store objectives, when not in use

## How it works

For a lightsheet microscope, objectives have to be exchanged on a regular base to enable different types of experiments. To have the objectives removed from the system safely stored, a container of universal size was designed. These containers keep the objectives safe from dust or spills. Further, the containers have the identifiers (magnification, NA, inventory number) engraved in the surface to be easily found.

## Authors

Mohammad Goudarzi, Imaging and Optics Facility (IOF), IST Austria, iof@ista.ac.at. 

Astrit Arslani, Machine Shop (MIBA), IST Austria, mshop@ist.ac.at.

## Acknowledgment

The MIBA Machine Shop of the IST Austria for production of the first version of the object, and allowing us to make it available for the public.
